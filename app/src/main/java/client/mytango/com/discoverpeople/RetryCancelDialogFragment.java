package client.mytango.com.discoverpeople;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import client.mytango.com.mytango.R;

public class RetryCancelDialogFragment extends DialogFragment {

    public RetryCancelDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_dialog_no_internet)
                .setPositiveButton(R.string.alert_dialog_retry,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((PeopleNearbyActivity)getActivity()).doPositiveClick();
                            }
                        }
                )
                .setNegativeButton(R.string.alert_dialog_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((PeopleNearbyActivity)getActivity()).doNegativeClick();
                            }
                        }
                )
                .create();
    }

}
