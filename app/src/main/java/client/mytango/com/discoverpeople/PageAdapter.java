package client.mytango.com.discoverpeople;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import client.mytango.com.dummydate.Comment;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.User;
import client.mytango.com.mytango.ProfileActivity;
import client.mytango.com.mytango.R;
import client.mytango.com.mytango.RoundedImageView;
import client.mytango.com.mytango.ShowImage;
import client.mytango.com.newsfeed.FeedAdapter;

/**
 * Created by huf on 02/12/2014.
 */
public class PageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    protected LayoutInflater inflater;
    List<Post> lPosts = new ArrayList<Post>();
    Context context;
    int curIndex = 0;
    Comment[] tComments;
    User user;
    private FeedAdapter.ItemClickListener itemClickListener;
    private HeadClickListener headClickListener;
    private FeedAdapter.RunnableAddedListener runnableAddedListener;
    private boolean enableEdit;

    public PageAdapter(Context context, List<Post> lPosts, User user, @NonNull FeedAdapter.ItemClickListener itemClickListener, @NonNull HeadClickListener headClickListener, @NonNull FeedAdapter.RunnableAddedListener runnableAddedListener, boolean enableEdit){
        this.lPosts = lPosts;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.user = user;
        this.itemClickListener = itemClickListener;
        this.headClickListener = headClickListener;
        this.runnableAddedListener = runnableAddedListener;
        this.enableEdit = enableEdit;
    }

    public interface HeadClickListener {
        public void headClicked(String viewType);
    }

    @Override
    public int getItemCount() {
        if(lPosts==null){
            return 1;
        }else{
            return lPosts.size() + 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if(viewType == TYPE_ITEM){
            View parent = inflater.inflate(R.layout.post_block, viewGroup, false);
            return FeedAdapter.ViewHolder.newInstance(parent);
        }else if(viewType == TYPE_HEADER){
            View parent = inflater.inflate(R.layout.page_head, viewGroup, false);
            return HeaderViewHolder.newInstance(parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if(viewHolder instanceof FeedAdapter.ViewHolder){
            final Post item = lPosts.get(position-1);

            ((FeedAdapter.ViewHolder)viewHolder).setHeadImage(user.getPhotoPath());
            ((FeedAdapter.ViewHolder)viewHolder).setName(item.getUserName());
            ((FeedAdapter.ViewHolder)viewHolder).setPostDate(item.getMillis());
            ((FeedAdapter.ViewHolder)viewHolder).setMediaContent(item.getMediaContent());
            ((FeedAdapter.ViewHolder)viewHolder).setLikes(item.getLikes());
            runnableAddedListener.runnableAdded(((FeedAdapter.ViewHolder)viewHolder).setComments(item.getComments()));

            ((FeedAdapter.ViewHolder)viewHolder).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.itemClicked(item.getUserName(), item.getMillis());
                }
            });
        }else if(viewHolder instanceof HeaderViewHolder){
            ((HeaderViewHolder)viewHolder).setProfile_name(user.getName());
            ((HeaderViewHolder)viewHolder).setHeadImage(user.getPhotoPath());
            ((HeaderViewHolder)viewHolder).setGoingOn(user.getGoingOn());
            ((HeaderViewHolder)viewHolder).enableEditable(enableEdit);

            if(enableEdit){
                ((HeaderViewHolder)viewHolder).setImageOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        headClickListener.headClicked(ProfileActivity.VIEW_TYPE_IMAGE);
                    }
                });
                ((HeaderViewHolder)viewHolder).setGoingOnOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        headClickListener.headClicked(ProfileActivity.VIEW_TYPE_GOINT_ON);
                    }
                });
            }
        }

    }

    public static final class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final View parent;
        private final TextView profile_name;
        private final RoundedImageView headImage;
        private final TextView goingOn;
        private final TextView settings;


        public static HeaderViewHolder newInstance(View parent) {
            TextView profile_name = (TextView)parent.findViewById(R.id.tv_user_name);
            RoundedImageView headImage = (RoundedImageView)parent.findViewById(R.id.profile_pic);
            TextView goingOn = (TextView)parent.findViewById(R.id.going_on);
            TextView settings = (TextView)parent.findViewById(R.id.settings);
            return new HeaderViewHolder(parent, profile_name, headImage, goingOn, settings);
        }

        private HeaderViewHolder(View parent, TextView profile_name, RoundedImageView headImage, TextView goingOn, TextView settings) {
            super(parent);
            this.parent = parent;
            this.profile_name = profile_name;
            this.headImage = headImage;
            this.goingOn = goingOn;
            this.settings = settings;
        }

        public void setProfile_name(String text) {
            profile_name.setText(text);
        }

        public void setHeadImage(String imagePath) {
            if(imagePath!=null) {
                new ShowImage().displayPhoto(headImage, imagePath);
            }
        }

        public void setGoingOn(String text) {
            goingOn.setText(text);
        }

        public void enableEditable(boolean enable){
            settings.setVisibility(enable?View.VISIBLE:View.GONE);
        }

        public void setImageOnClickListener(View.OnClickListener listener) {
            headImage.setOnClickListener(listener);
        }
        public void setGoingOnOnClickListener(View.OnClickListener listener) {
            goingOn.setOnClickListener(listener);
        }

    }

}
