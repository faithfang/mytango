/*
 * Copyright � 2003-2013 Monitise Group Limited. All Rights Reserved. 
 * 
 * Save to the extent permitted by law, you may not use, copy, modify, 
 * distribute or create derivative works of this material or any part 
 * of it without the prior written consent of Monitise Group Limited.  
 * Any reproduction of this material must contain this notice.
 *
 */

package client.mytango.com.discoverpeople;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import client.mytango.com.mytango.R;
/**
 * Stops user input and displays a progress circle icon
 * 
 * @author savagep
 */

public class ModalWaitView extends FrameLayout {

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public ModalWaitView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public ModalWaitView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);

	}

	/**
	 * @param context
	 */
	public ModalWaitView(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.wait_screen, this, true);

		setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// Block user input
				return true;
			}
		});
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
