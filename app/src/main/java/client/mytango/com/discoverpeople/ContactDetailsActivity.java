package client.mytango.com.discoverpeople;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import client.mytango.com.mytango.R;


public class ContactDetailsActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks{

    public static final int CONTACTS_DETAILS_LOADER_ID = 10002;

    String mCurFilter;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        textView = (TextView)findViewById(R.id.textView);
        getLoaderManager().initLoader(CONTACTS_DETAILS_LOADER_ID, getIntent().getExtras(), this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()){
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.
        Uri baseUri;
        if (mCurFilter != null) {
            baseUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI,
                    Uri.encode(mCurFilter));
        } else {
            baseUri = ContactsContract.Contacts.CONTENT_URI;
        }

        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        String select = "(" + ContactsContract.Contacts.DISPLAY_NAME + "= \"" + args.getString(ContactsContract.Contacts.DISPLAY_NAME) + "\")";
        return new CursorLoader(this, baseUri,
                ContactsListActivity.CONTACTS_SUMMARY_PROJECTION, select, null,
                ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        try{
            if (((Cursor)data).moveToFirst()) {
                textView.setText(((Cursor) data).getString(((Cursor) data).getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
            }
        }catch (Exception e){

        }

    }

    @Override
    public void onLoaderReset(Loader loader) {
        textView.setText(null);
    }
}
