/*
 * Copyright © 2003-2013 Monitise Group Limited. All Rights Reserved.
 *
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Monitise Group Limited.
 * Any reproduction of this material must contain this notice.
 */

package client.mytango.com.discoverpeople;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class MapWrapperLayout extends RelativeLayout {

	private View fake_info_window_linear_layout;

	public MapWrapperLayout(Context context) {
		super(context);
	}

	public MapWrapperLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MapWrapperLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}



	public void setFakeInfoWindow(View fakeInfoWindowLinearLayout) {
		fake_info_window_linear_layout = fakeInfoWindowLinearLayout;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean fake_ret = false;
		if (fake_info_window_linear_layout != null && fake_info_window_linear_layout.getVisibility() == View.VISIBLE) {
			fake_ret = fake_info_window_linear_layout.dispatchTouchEvent(ev);
		}

		if ((fake_info_window_linear_layout != null)
				&& (fake_info_window_linear_layout.getVisibility() == View.VISIBLE)
				&& (ev.getActionMasked() == MotionEvent.ACTION_UP)) {
			fake_info_window_linear_layout.clearAnimation();
			fake_info_window_linear_layout.setVisibility(View.GONE);
		}
		// If the infoWindow consumed the touch event, then just return true.
		// Otherwise pass this event to the super class and return it's result
		return fake_ret || super.dispatchTouchEvent(ev);
	}
}
