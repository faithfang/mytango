package client.mytango.com.discoverpeople;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.User;
import client.mytango.com.dummydate.Users;
import client.mytango.com.mytango.R;

/**
 * Created by fangfanghu on 14/12/2014.
 */
public class NearbyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private User[] usersArray;
    private Map<String, Integer> userPosts;
    private Map<String, Double> userDistances;
    private ItemClickListener itemClickListener;
    protected LayoutInflater inflater;


    public NearbyListAdapter(Context context, Users users, Map<String, Integer> userPosts, Map<String, Double> userDistances, @NonNull ItemClickListener itemClickListener){
        this.usersArray = users.getUsers();
        this.userPosts = userPosts;
        this.userDistances = userDistances;
        this.itemClickListener = itemClickListener;
        inflater = LayoutInflater.from(context);
    }

    public interface ItemClickListener {
        public void itemClicked(String name);
    }

    @Override
    public int getItemCount() {
        if(usersArray==null){
            return 0;
        }else{
            return usersArray.length;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View parent = inflater.inflate(R.layout.nearby_list_item, viewGroup, false);
        return ItemViewHolder.newInstance(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        final User item = usersArray[position];

        ((ItemViewHolder)viewHolder).setName(item.getName());
        ((ItemViewHolder)viewHolder).setDistance(String.valueOf(userDistances.get(item.getName())));
        ((ItemViewHolder)viewHolder).setGoingOn(item.getGoingOn());
        ((ItemViewHolder)viewHolder).setPosts(String.valueOf(userPosts.get(item.getName())));

        ((ItemViewHolder)viewHolder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.itemClicked(item.getName());
            }
        });

    }

    public static final class ItemViewHolder extends RecyclerView.ViewHolder {
        private final View parent;
        private final ImageView image;
        private final TextView name;
        private final TextView distance;
        private final TextView goingOn;
        private final TextView posts;

        public static ItemViewHolder newInstance(View parent) {

            ImageView image = (ImageView)parent.findViewById(R.id.pic);
            TextView name = (TextView)parent.findViewById(R.id.name);
            TextView distance = (TextView)parent.findViewById(R.id.distance);
            TextView goingOn = (TextView)parent.findViewById(R.id.goingOn);
            TextView posts = (TextView)parent.findViewById(R.id.posts);

            return new ItemViewHolder(parent, image, name, distance, goingOn, posts);
        }

        private ItemViewHolder(View parent, ImageView image, TextView name, TextView distance, TextView goingOn, TextView posts) {
            super(parent);
            this.parent = parent;
            this.image = image;
            this.name = name;
            this.distance = distance;
            this.goingOn = goingOn;
            this.posts = posts;
        }

        //TODO set image when image format is known

        public void setName(String text) {
            name.setText(text);
        }

        public void setDistance(String text) {
            distance.setText(text + " km");
        }
        public void setGoingOn(String text) {
            goingOn.setText(text);
        }
        public void setPosts(String text) {
            posts.setText(text + " Posts");
        }


        public void setOnClickListener(View.OnClickListener listener) {
            parent.setOnClickListener(listener);
        }
    }
}
