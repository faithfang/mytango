package client.mytango.com.discoverpeople;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import client.mytango.com.mytango.BuildConfig;
import client.mytango.com.mytango.R;

public class PeopleNearbyActivity extends ActionBarActivity implements PeopleNearbyMapFragment.OnMapFragmentInteractionListener{

    private DialogFragment dialog;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private boolean showList = false;

    @Override
    public void onMapFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_nearby);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_root, Fragment.instantiate(this, PeopleNearbyMapFragment.class.getName(), getIntent().getExtras()))
                .commit();

        ((ImageView)findViewById(R.id.toggle)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showList = !showList;
                if(showList){
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.activity_root, Fragment.instantiate(PeopleNearbyActivity.this, PeopleNearByListFragment.class.getName(), getIntent().getExtras()))
                            .commit();
                }else{
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.activity_root, Fragment.instantiate(PeopleNearbyActivity.this, PeopleNearbyMapFragment.class.getName(), getIntent().getExtras()))
                            .commit();
                }

            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        // Dialogs are more reliable when launched here.
        checkConnectivity();

    }

    private void checkConnectivity() {

        boolean networkPresent = isNetworkAvailable();

        if (!networkPresent) {
            showRetryCancelDialog();
        }else if(!checkPlayServices()){

        }else{

//            new SplashScreenUpgradeSupport(this).checkUpgrade();
        }
    }

    protected boolean checkPlayServices() {

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    public boolean isNetworkAvailable() {

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork !=
                null && activeNetwork.isConnectedOrConnecting();

        return isConnected;

    }

    private void showRetryCancelDialog() {
        dialog = new RetryCancelDialogFragment();
        dialog.show(getSupportFragmentManager(), null);
    }

    public void doPositiveClick() {
        if (BuildConfig.DEBUG)	 Log.i("FragmentAlertDialog", "Positive click!");

        dialog.dismiss();
        checkConnectivity();
    }

    public void doNegativeClick() {
        if (BuildConfig.DEBUG)	 Log.i("FragmentAlertDialog", "Negative click!");

        dialog.dismiss();
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.people_nearby, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()){
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
