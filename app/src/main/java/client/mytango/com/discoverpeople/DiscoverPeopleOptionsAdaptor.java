package client.mytango.com.discoverpeople;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import client.mytango.com.mytango.R;

/**
 * Created by fangfanghu on 30/11/2014.
 */
public class DiscoverPeopleOptionsAdaptor extends BaseAdapter {
    private String[] items;
    private Context context;
    private OptionClickListener optionClickListener;

    public DiscoverPeopleOptionsAdaptor(Context context, String[] items, OptionClickListener optionClickListener){
        this.context = context;
        this.items = items;
        this.optionClickListener = optionClickListener;
    }

    public interface OptionClickListener {
        public void optionClicked(int optionId);
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           View v = mInflater.inflate(R.layout.two_way_view_item, null);

        if(position==0){
            ((ImageView)v.findViewById(R.id.image)).setImageResource(R.drawable.ic_action_group);
            ((TextView)v.findViewById(R.id.text)).setText(items[position]);
        }else{
            ((ImageView)v.findViewById(R.id.image)).setImageResource(R.drawable.ic_action_place);
            ((TextView)v.findViewById(R.id.text)).setText(items[position]);
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionClickListener.optionClicked(position);
            }
        });

        return v;
    }
}
