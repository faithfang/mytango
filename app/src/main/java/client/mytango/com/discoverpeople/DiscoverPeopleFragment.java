package client.mytango.com.discoverpeople;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.lucasr.twowayview.TwoWayView;

import client.mytango.com.mytango.BaseActivity;
import client.mytango.com.mytango.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class DiscoverPeopleFragment extends Fragment implements DiscoverPeopleOptionsAdaptor.OptionClickListener{


    public DiscoverPeopleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_discover_people, container, false);

        TwoWayView twoWayView = (TwoWayView) view.findViewById(R.id.twowayview);

        String[] items = {"My Contacts", "People Nearby"};
        DiscoverPeopleOptionsAdaptor adaptor = new DiscoverPeopleOptionsAdaptor(getActivity(), items, this);

        twoWayView.setAdapter(adaptor);
        return view;
    }

    @Override
    public void optionClicked(int id) {
        switch(id){
            case 0:
                Intent list = new Intent(getActivity(), ContactsListActivity.class);
                startActivity(list);

                break;
            case 1:
                Intent nearbyMap = new Intent(getActivity(), PeopleNearbyActivity.class);
                startActivity(nearbyMap);
                break;
            default:
                break;
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((BaseActivity) activity).onSectionAttached(
                getArguments().getInt(BaseActivity.ARG_SECTION_NUMBER));
    }
}
