package client.mytango.com.discoverpeople;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.Posts;
import client.mytango.com.dummydate.User;
import client.mytango.com.dummydate.Users;
import client.mytango.com.mytango.ProfileActivity;
import client.mytango.com.mytango.R;
import client.mytango.com.networking.ApiManager;
import rx.Observable;
import rx.android.concurrency.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by fangfanghu on 14/12/2014.
 */
public class PeopleNearByListFragment extends Fragment implements NearbyListAdapter.ItemClickListener{

    private RecyclerView recyclerView;

    private Users mUsers;
    private Posts mPosts;

    LatLng londonLibrary = new LatLng(51.507276, -0.137479);

    public PeopleNearByListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_people_nearby_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        //get users
        //get posts

        Observable.from(londonLibrary)
                .flatMap(new Func1<LatLng, Observable<Users>>() {
                    @Override
                    public Observable<Users> call(LatLng latLng) {
                        return ApiManager.getNearbyUsers(latLng);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Users>() {
                    @Override
                    public void call(Users users) {
                        // do your work
                        mUsers = users;
                        updateUI();
                    }
                });

        Observable.from("")
                .flatMap(new Func1<String, Observable<Posts>>() {
                    @Override
                    public Observable<Posts> call(String s) {
                        return ApiManager.getPostsData(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Posts>() {
                    @Override
                    public void call(Posts posts) {
                        // do your work
                        mPosts = posts;
                        updateUI();
                    }
                });
    }

    private void updateUI(){
        if(mUsers==null||mPosts==null){
            return;
        }
        //plant pins
        //move camera

        Map<String, Integer> userPosts = new HashMap<String, Integer>();
        for(User u: mUsers.getUsers()){
            userPosts.put(u.getName(),0);
        }

        //TODO optimize the searching and replacing
        for(Post p: mPosts.getPosts()){
            if(userPosts.containsKey(p.getUserName())){
                int posts = userPosts.get(p.getUserName());
                posts++;
                userPosts.put(p.getUserName(), (Integer)posts);
            }
        }

        Map<String, Double> userDistances = new HashMap<String, Double>();

        for(User u: mUsers.getUsers()){
            //u.getLocation() - calculate the distance
            userDistances.put(u.getName(), new Double(1.0));

        }

        NearbyListAdapter adapter = new NearbyListAdapter(getActivity(), mUsers, userPosts,userDistances, this);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void itemClicked(String name) {
        Intent detail = new Intent(getActivity(), ProfileActivity.class);
        detail.putExtra(ProfileActivity.KEY_PROFILE_NAME, name);
        detail.putExtra(ProfileActivity.KEY_ALLOW_EDIT, false);
        startActivity(detail);
    }
}
