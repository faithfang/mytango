package client.mytango.com.discoverpeople;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Map;

import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.Posts;
import client.mytango.com.dummydate.User;
import client.mytango.com.dummydate.Users;
import client.mytango.com.mytango.ProfileActivity;
import client.mytango.com.mytango.R;
import client.mytango.com.networking.ApiManager;
import rx.Observable;
import rx.android.concurrency.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PeopleNearbyMapFragment.OnMapFragmentInteractionListener} interface
 * to handle interaction events.
 *
 */
public class PeopleNearbyMapFragment extends Fragment {

    private OnMapFragmentInteractionListener mListener;

    private SupportMapFragment supportMapFragment;
    private GoogleMap map;
    private ViewGroup infoWindow;
    private MapWrapperLayout mapWrapperLayout;
    private LinearLayout custom_info_window_ll;

    private Users mUsers;
    private Posts mPosts;

    LatLng londonLibrary = new LatLng(51.507276, -0.137479);

    public PeopleNearbyMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View parent = inflater.inflate(R.layout.fragment_people_nearby_map, container, false);

        return parent;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boolean isNearMe = false;

        LatLng initLoc = getDefaultLocation();
        int initialZoom = 4;

        if (isNearMe) {
            initialZoom = 10;
        }

        if (supportMapFragment == null) {

//            CameraPosition cameraPos = new CameraPosition.Builder().target(initLoc).zoom(initialZoom).build();
//
//            supportMapFragment = SupportMapFragment.newInstance(new GoogleMapOptions().camera(cameraPos));

            supportMapFragment = SupportMapFragment.newInstance();

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.map_root, supportMapFragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        map = supportMapFragment.getMap();
        if(map!=null){
            map.setMyLocationEnabled(true);
        }

        //get users
        //get posts

        Observable.from(londonLibrary)
                .flatMap(new Func1<LatLng, Observable<Users>>() {
                    @Override
                    public Observable<Users> call(LatLng latLng) {
                        return ApiManager.getNearbyUsers(latLng);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Users>() {
                    @Override
                    public void call(Users users) {
                        // do your work
                        mUsers = users;
                        updateUI();
                    }
                });

        Observable.from("")
                .flatMap(new Func1<String, Observable<Posts>>() {
                    @Override
                    public Observable<Posts> call(String s) {
                        return ApiManager.getPostsData(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Posts>() {
                    @Override
                    public void call(Posts posts) {
                        // do your work
                        mPosts = posts;
                        updateUI();
                    }
                });
    }

    private void updateUI(){
        if(mUsers==null||mPosts==null){
            return;
        }
        //plant pins
        //move camera

        final Map<String, Integer> userPosts = new HashMap<String, Integer>();
        for(User u: mUsers.getUsers()){
            userPosts.put(u.getName(),0);
        }

        //TODO optimize the searching and replacing
        for(Post p: mPosts.getPosts()){
            if(userPosts.containsKey(p.getUserName())){
                int posts = userPosts.get(p.getUserName());
                posts++;
                userPosts.put(p.getUserName(), (Integer)posts);
            }
        }

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds.Builder nearBysBuilder = new LatLngBounds.Builder();

                for(User u: mUsers.getUsers()){
                    map.addMarker(new MarkerOptions()
                            .position(u.getLocation())
                            .title(u.getName())
                            .snippet(u.getGoingOn() + " " + String.valueOf(userPosts.get(u.getName())) + " Posts"));
                    nearBysBuilder.include(u.getLocation());
                }
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(nearBysBuilder.build(), 100));

                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent i = new Intent(getActivity(), ProfileActivity.class);
                        i.putExtra(ProfileActivity.KEY_PROFILE_NAME, marker.getTitle());
                        i.putExtra(ProfileActivity.KEY_ALLOW_EDIT, false);
                        startActivity(i);
                    }
                });
            }
        });
    }

    private LatLng getDefaultLocation() {

        //TODO getlocation using Map Location service
//        Location loc = MapServices.getInstance(getActivity()).getLastLocation();
        Location loc = null;
        if (loc == null) {
            return londonLibrary;
        }

        return new LatLng(loc.getLatitude(), loc.getLongitude());

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onMapFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMapFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMapFragmentInteractionListener {
        public void onMapFragmentInteraction(Uri uri);
    }

}
