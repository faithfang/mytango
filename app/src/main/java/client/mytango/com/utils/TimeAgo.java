package client.mytango.com.utils;

import java.util.Date;

/**
 * Created by huf on 19/12/2014.
 */
public class TimeAgo {
    public static String getTimeAgo(long millis){
        Date d = new Date();
        long dif = d.getTime() - millis;
        String ago = null;
        if(dif/(30*24*60*60*1000)>1){
            ago = String.valueOf(dif/(30*24*60*60*1000)+1) + " months ago";
        }else if((dif/24*60*60*1000)>1){
            ago = String.valueOf(dif/(24*60*60*1000)+1) + " days ago";
        }else if((dif/60*60*1000)>1){
            ago = String.valueOf(dif/(60*60*1000)+1) + " hours ago";
        }else if((dif/60*1000)>1){
            ago = String.valueOf(dif/(60*1000)+1) + " minutes ago";
        }else if((dif/1000)>1){
            ago = String.valueOf(dif/(1000)+1) + " seconds ago";
        }
        return ago;
    }
}
