package client.mytango.com.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by fangfanghu on 03/01/15.
 */
public class Prefs {
    private static final String PREFS_NAME = "prefs";
    public static final String PREFS_KEY_POSTS_LIST = "posts";
    public static final String PREFS_KEY_USERS_LIST = "users";

    private Application context;

    public Prefs(Application context) {
        this.context = context;
    }

    private SharedPreferences.Editor edit() {
        return getSharedPreferences().edit();
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void set(String key, String value) {
        edit().putString(key, value).commit();
    }

    public String get(String key){
        return getSharedPreferences().getString(key, null);
    }
}
