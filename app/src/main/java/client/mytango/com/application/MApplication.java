package client.mytango.com.application;

import android.app.Application;

/**
 * Created by fangfanghu on 03/01/15.
 */
public class MApplication extends Application {
    private static MApplication instance;
    private Prefs prefs;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        prefs = new Prefs(this);
    }

    public static MApplication getInstance() {
        return instance;
    }

    public Prefs getPrefs() {
        return prefs;
    }
}
