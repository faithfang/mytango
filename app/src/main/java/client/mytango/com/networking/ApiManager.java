package client.mytango.com.networking;

import com.google.android.gms.maps.model.LatLng;

import client.mytango.com.application.MApplication;
import client.mytango.com.application.Prefs;
import client.mytango.com.dummydataset.FakePostsManager;
import client.mytango.com.dummydataset.FakeUsersManager;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.Posts;
import client.mytango.com.dummydate.User;
import client.mytango.com.dummydate.Users;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by fangfanghu on 24/11/2014.
 */
public class ApiManager {

    private interface ApiManagerService {
        @GET("/user")
        User getUser(@Query("name") String name);
        @GET("/users/list")
        Users getUsers(@Query("name") String name);
        @GET("/posts")
        Posts getPosts(@Query("name") String name);
        @GET("/posts/list")
        Posts getPostsByName(@Query("name") String name);
        @GET("/nearby/users/list")
        Users getNearbyUsers(@Query("latLng") LatLng latLng);
        @POST("/posts/new")
        void createPost(@Body Post post);
        @PUT("/user/{username}")
        void updateUserPhotoPath(@Path("username") String username, @Body String photopath);
    }

    //TODO use live server later
    private static final RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://api.openweathermap.org/data/2.5")
            .build();
//    private static final ApiManagerService apiManager = restAdapter.create(ApiManagerService.class);


    public static final ApiManagerService apiManager = new ApiManagerService() {
        @Override
        public User getUser(@Query("name") String name) {
            return FakeUsersManager.getInstance().getUser(name);
        }

        @Override
        public Users getUsers(@Query("name") String name) {
            return FakeUsersManager.getInstance().getUsers();
        }

        @Override
        public Posts getPosts(@Query("name") String name) {
            return FakePostsManager.getInstance().getPosts();
        }

        @Override
        public Posts getPostsByName(@Query("name") String name) {
            return FakePostsManager.getInstance().getPostsByName(name);
        }

        @Override
        public Users getNearbyUsers(@Query("latLng") LatLng latLng) {
            //TODO filter user by location
            return FakeUsersManager.getInstance().getUsers();
        }

        @Override
        public void createPost(@Body Post post) {
            FakePostsManager.getInstance().addPost(post);
        }

        @Override
        public void updateUserPhotoPath(@Path("username") String username, @Body String photopath){
            FakeUsersManager.getInstance().updateUserPhotoPath(username, photopath);
        }
    };

    public static void updateUserPhotoPathData(String username, String photopath) {
        apiManager.updateUserPhotoPath(username, photopath);
    }
    public static void createPostData(final Post post) {
       apiManager.createPost(post);
    }

    public static Observable<User> getUserData(final String name) {
        return Observable.create(new Observable.OnSubscribe<User>() {
            @Override
            public void call(Subscriber<? super User> subscriber) {
                try {
                    subscriber.onNext(apiManager.getUser(name));
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<Users> getUsersData(final String fakeName) {
        return Observable.create(new Observable.OnSubscribe<Users>() {
            @Override
            public void call(Subscriber<? super Users> subscriber) {
                try {
                    subscriber.onNext(apiManager.getUsers(fakeName));
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<Posts> getPostsData(final String fakeName) {
        return Observable.create(new Observable.OnSubscribe<Posts>() {
            @Override
            public void call(Subscriber<? super Posts> subscriber) {
                try {
                    subscriber.onNext(apiManager.getPosts(fakeName));
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<Posts> getPostsDataByName(final String name) {
        return Observable.create(new Observable.OnSubscribe<Posts>() {
            @Override
            public void call(Subscriber<? super Posts> subscriber) {
                try {
                    subscriber.onNext(apiManager.getPostsByName(name));
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<Users> getNearbyUsers(final LatLng latLng) {
        return Observable.create(new Observable.OnSubscribe<Users>() {
            @Override
            public void call(Subscriber<? super Users> subscriber) {
                try {
                    subscriber.onNext(apiManager.getNearbyUsers(latLng));
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }
}
