package client.mytango.com.dummydate;

/**
 * Created by huf on 24/11/2014.
 */
public class Post implements Comparable<Post>{
    private Comment[] comments;
    private MediaContent mediaContent;
    private Integer likes;
    private long millis = -1;
    private String userName;

    public Post(){

    }

    @Override
    public int compareTo(Post another) {

        return new Long(millis).compareTo(new Long(another.getMillis()));
    }

    public Post(String userName, long millis, MediaContent mediaContent, Integer likes, Comment[] comments){
        this.userName = userName;
        this.millis = millis;
        this.mediaContent = mediaContent;
        this.likes = likes;
        this.comments = comments;
    }

    public void setComments(Comment[] comments){
        this.comments = comments;
    }

    public void setMediaContent(MediaContent mediaContent){
        this.mediaContent = mediaContent;
    }

    public void setLikes(Integer likes){
        this.likes = likes;
    }

    public void setMillis(long millis){
        this.millis = millis;
    }

    public void setUserName(String userName){
        this.userName = userName;
    }

    public Comment[] getComments(){
        return comments;
    }

    public MediaContent getMediaContent(){
        return mediaContent;
    }

    public Integer getLikes(){
        return likes;
    }

    public long getMillis(){
        return millis;
    }

    public String getUserName(){
        return userName;
    }
}
