package client.mytango.com.dummydate;

/**
 * Created by huf on 24/11/2014.
 */
public class Comment implements Comparable<Comment>{

    private String name;
    private String said;
    private long millis = -1;

    public Comment(String name, String said, long millis){
        this.name = name;
        this.said = said;
        this.millis = millis;
    }

    @Override
    public int compareTo(Comment another) {

        return new Long(millis).compareTo(new Long(another.getMillis()));
    }

    public String getName(){
        return name;
    }

    public String getSaid(){
        return said;
    }

    public long getMillis(){
        return millis;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setSaid(String said){
        this.said = said;
    }

    public void setMillis(long millis){
        this.millis = millis;
    }
}
