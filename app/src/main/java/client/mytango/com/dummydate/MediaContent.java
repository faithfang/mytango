package client.mytango.com.dummydate;

import android.net.Uri;

/**
 * Created by huf on 24/11/2014.
 */
public class MediaContent {
    private String plainText;
    private String imagePath;
    private Uri videoUri;

    public MediaContent(String plainText, String imagePath, Uri videoUri){
        this.plainText = plainText;
        this.imagePath = imagePath;
        this.videoUri = videoUri;
    }

    public void setPlainText(String plainText){
        this.plainText = plainText;
    }

    public void setImagePath(String imagePath){
        this.imagePath = imagePath;
    }

    public void setVideoUri(Uri videoUri){
        this.videoUri = videoUri;
    }

    public String getPlainText(){
        return plainText;
    }

    public String getImagePath(){
        return imagePath;
    }

    public Uri getVideoUri(){
        return videoUri;
    }
}
