package client.mytango.com.dummydate;

import client.mytango.com.dummydate.User;

/**
 * Created by huf on 24/11/2014.
 */
public class Users {
    private User[] users;

    public Users(User[] users){
        this.users = users;
    }

    public User[] getUsers(){
        return users;
    }

    public void setUsers(User[] users){
        this.users = users;
    }
}
