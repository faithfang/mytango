package client.mytango.com.dummydate;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by huf on 24/11/2014.
 */
public class User {
    private String name;
    private String photoPath;
    private String description;
    private String[] followingNames;
    private String goingOn;
    private LatLng location;

    public User(String name, String photoPath, String description, String[] followingNames, String goingOn, LatLng location){
        this.name = name;
        this.photoPath = photoPath;
        this.description = description;
        this.followingNames = followingNames;
        this.goingOn = goingOn;
        this.location = location;
    }

    public LatLng getLocation(){return location;}

    public void setLocation(LatLng location){this.location = location;}

    public String getGoingOn() {return goingOn;}

    public void setGoingOn(String goingOn){
        this.goingOn = goingOn;
    }

    public String getName(){
        return name;
    }

    public String getPhotoPath(){
        return  photoPath;
    }

    public String getDescription(){
        return description;
    }

    public String[] getFollowingNames(){
        return followingNames;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPhotoPath(String photoPath){
        this.photoPath = photoPath;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setFollowingNames(String[] followingNames){
        this.followingNames = followingNames;
    }
}
