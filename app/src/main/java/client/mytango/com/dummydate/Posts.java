package client.mytango.com.dummydate;

import client.mytango.com.dummydate.Post;

/**
 * Created by huf on 24/11/2014.
 */
public class Posts {
    private Post[] posts;

    public Posts(Post[] posts){
        this.posts = posts;
    }

    public Post[] getPosts(){
        return posts;
    }

    public void setPosts(Post[] posts){
        this.posts = posts;
    }
}
