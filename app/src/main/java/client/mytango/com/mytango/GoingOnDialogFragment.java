package client.mytango.com.mytango;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by huf on 28/10/2014.
 */
public class GoingOnDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Status");
        final View dialogFragmentView = inflater.inflate(R.layout.dialog_going_on, container, false);
        Button dialog_btn_cancel = (Button) dialogFragmentView.findViewById(R.id.dialog_btn_cancel);
        dialog_btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        Button dialog_btn_save = (Button) dialogFragmentView.findViewById(R.id.dialog_btn_save);
        dialog_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et_going_on = (EditText) dialogFragmentView.findViewById(R.id.et_going_on);
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(ProfileActivity.PREFS_GOING_ON, et_going_on.getText().toString());
                editor.commit();
                ((ProfileActivity)getActivity()).displayGoingOn();
                getDialog().dismiss();
            }
        });

        return dialogFragmentView;
    }
}
