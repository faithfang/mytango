package client.mytango.com.mytango;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import client.mytango.com.discoverpeople.PageAdapter;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.Posts;
import client.mytango.com.dummydate.User;
import client.mytango.com.networking.ApiManager;
import client.mytango.com.newsfeed.FeedAdapter;
import client.mytango.com.newsfeed.FeedDetailActivity;
import client.mytango.com.newsfeed.NewsFeedChannelsFragment;
import rx.Observable;
import rx.android.concurrency.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by huf on 27/10/2014.
 */
public class ProfileActivity extends ActionBarActivity implements FeedAdapter.ItemClickListener, PageAdapter.HeadClickListener, FeedAdapter.RunnableAddedListener, CreatePostDialogFragment.onPostCreatedListener{
    public static final String PREFS_GOING_ON = "GoingOn";
    public static final String PREFS_PROFILE_PHOTO_PATH = "ProfilePhotoPath";
    public static final String KEY_PROFILE_NAME = "KeyProfileName";
    public static final String KEY_ALLOW_EDIT = "KeyAllowEdit";

    public static final String VIEW_TYPE_IMAGE = "Image";
    public static final String VIEW_TYPE_GOINT_ON = "GoingOn";

    private final int SELECT_IMAGE_FROM_GALLERY = 2;
    private final int SELECT_IMAGE_FROM_CAMERA = 1;
    private ImageView profile_pic;
    private TextView going_on;

    private Posts mPosts;
    private User me;
    private RecyclerView recyclerView;

    // A queue of Runnables
    private final BlockingQueue<Runnable> mDecodeWorkQueue = new LinkedBlockingQueue<Runnable>();

    private TimerTask task;
    private Timer timer;

    private String profilePhotoPath;

    private String profileName;
    private boolean allowEdit;

    private File createImageFile() throws IOException {
        // Create an image file name
        File imageF = CreateImageFile.getInstance().getImageFile();
        profilePhotoPath = imageF.getAbsolutePath();
        return imageF;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if(getIntent()!=null&&getIntent().getExtras()!=null){
            Bundle extras = getIntent().getExtras();
            profileName = extras.getString(KEY_PROFILE_NAME);
            allowEdit = extras.getBoolean(KEY_ALLOW_EDIT);
        }


        profile_pic = (ImageView) findViewById(R.id.profile_pic);
//        profile_pic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectImage();
//            }
//        });
//
//        going_on = (TextView) findViewById(R.id.going_on);
//        going_on.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                updateGoingOn();
//            }
//        });

        ImageView createPost = (ImageView) findViewById(R.id.imageView2);
        createPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPost();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        if(!allowEdit){
            createPost.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        Observable.from(profileName)
                .flatMap(new Func1<String, Observable<User>>() {
                    @Override
                    public Observable<User> call(String s) {
                        return ApiManager.getUserData(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User user) {
                        // do your work

                        if(user!=null){
                            me = user;
                        }else{
                            me = new User(profileName, null, null, null, null, null);
                        }
                        updateUI();
                    }
                });

        Observable.from(profileName)
                .flatMap(new Func1<String, Observable<Posts>>() {
                    @Override
                    public Observable<Posts> call(String s) {
                        return ApiManager.getPostsDataByName(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Posts>() {
                    @Override
                    public void call(Posts posts) {
                        // do your work
                        if(posts!=null){
                            mPosts = posts;
                        }else{
                            mPosts = new Posts(null);
                        }
                        updateUI();

                    }
                });

    }

    private void updateUI() {
        if (me == null || mPosts == null) {
            return;
        }

        recyclerView.setVisibility(View.VISIBLE);
            List<Post> postsList = null;
            if(mPosts!=null && mPosts.getPosts()!=null){
                postsList = Arrays.asList(mPosts.getPosts());
                Collections.sort(postsList);
                Collections.reverse(postsList);
            }

            PageAdapter adapter = new PageAdapter(ProfileActivity.this, postsList, me, ProfileActivity.this, ProfileActivity.this, ProfileActivity.this, allowEdit);
            recyclerView.setAdapter(adapter);

            task = new TimerTask(){
                public void run() {
                    if(mDecodeWorkQueue.size()!=0){
                        Iterator itr = mDecodeWorkQueue.iterator();
                        while(itr.hasNext()) {
                            try{
                                Runnable element = (Runnable)itr.next();
                                if(element!=null){
                                    runOnUiThread(element);
                                }
                            }catch (Exception e){

                            }

                        }
                    }
                }
            };

            timer = new Timer();
            timer.scheduleAtFixedRate(task, 0, 4000);


    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer!=null){
            timer.cancel();
        }
    }

    @Override
    public void itemClicked(String name, long millis) {
        Intent detailIntent = new Intent(this, FeedDetailActivity.class);
        detailIntent.putExtra(NewsFeedChannelsFragment.KEY_POST_USER_NAME, name);
        detailIntent.putExtra(NewsFeedChannelsFragment.KEY_POST_MILLIS, millis);
        startActivity(detailIntent);
    }

    @Override
    public void headClicked(String viewType) {
//        Intent detailIntent = new Intent(FeedListActivity.this, FeedDetailActivity.class);
//        detailIntent.putExtra(FeedDetailActivity.ARG_ITEM, item);
//        startActivity(detailIntent);

        if(viewType.equals(VIEW_TYPE_IMAGE)){
            Toast.makeText(this, "Image Clicked", Toast.LENGTH_SHORT).show();
            selectImage();

        }else if(viewType.equals(VIEW_TYPE_GOINT_ON)){
            Toast.makeText(this, "GoingOn Clicked", Toast.LENGTH_SHORT).show();
            updateGoingOn();
        }

    }

    @Override
    public void runnableAdded(Runnable item) {
//        Intent detailIntent = new Intent(FeedListActivity.this, FeedDetailActivity.class);
//        detailIntent.putExtra(FeedDetailActivity.ARG_ITEM, item);
//        startActivity(detailIntent);
        try{
            mDecodeWorkQueue.add(item);
        }catch (Exception e) {

        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        displayProfilePhoto();
    }

    private void createPost(){
        CreatePostDialogFragment createPostDialogFragment = new CreatePostDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_PROFILE_NAME, profileName);
        createPostDialogFragment.setArguments(bundle);
        createPostDialogFragment.show(this.getSupportFragmentManager(), null);
    }

    private void displayProfilePhoto() {
        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
        String savedProfilePhotoPath = settings.getString(PREFS_PROFILE_PHOTO_PATH, null);

        if(savedProfilePhotoPath!=null){
//            Picasso.with(this).load(savedProfilePhotoPath).resize(60, 60)
//                    .centerCrop().into(profile_pic);
            new ShowImage().displayPhoto(profile_pic, savedProfilePhotoPath);
        }
    }

    public void displayGoingOn() {
        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
        String s_going_on = settings.getString(PREFS_GOING_ON, "");
        going_on.setText(s_going_on);
    }

    private void updateGoingOn() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
//        builder.setTitle("Status");
//        final View view = View.inflate(this, R.layout.dialog_going_on, null);
//        builder.setView(view);
//        builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        })
//                .setPositiveButton(R.string.dialog_save, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        EditText et_going_on = (EditText) view.findViewById(R.id.et_going_on);
//                        getSharedPreferences(PREFS_NAME, 0).edit().putString(PREFS_GOING_ON, et_going_on.getText().toString()).commit();
//                    }
//                });
//
//        builder.show();
        GoingOnDialogFragment goingOnDialogFragment = new GoingOnDialogFragment();
        goingOnDialogFragment.show(this.getSupportFragmentManager(), null);
    }

    private void selectImage(){
        final String take_photo = getResources().getString(R.string.option_take_photo);
        final String from_gallery = getResources().getString(R.string.option_from_gallery);
        final String cancel = getResources().getString(R.string.option_cancel);
        String add_photo = getResources().getString(R.string.title_add_photo);
        final CharSequence[] options = {take_photo, from_gallery, cancel };

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(take_photo))
                {
                    try {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(createImageFile()));
                        startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                }
                else if (options[item].equals(from_gallery))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_IMAGE_FROM_GALLERY);

                }
                else if (options[item].equals(cancel)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_IMAGE_FROM_CAMERA) {
                if(profilePhotoPath!=null){
                    saveProfilePhotoPath();
                }
            } else if (requestCode == SELECT_IMAGE_FROM_GALLERY) {

                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                profilePhotoPath = picturePath;
                saveProfilePhotoPath();
            }
        }
    }

    private void saveProfilePhotoPath() {
          ApiManager.updateUserPhotoPathData(profileName, profilePhotoPath);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostCreated() {
        onResume();
    }
}
