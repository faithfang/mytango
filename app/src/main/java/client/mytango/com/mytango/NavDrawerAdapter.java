package client.mytango.com.mytango;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by huf on 27/10/2014.
 */
public class NavDrawerAdapter extends BaseAdapter {
    private NavDest[] navItems = {NavDest.HEADER, NavDest.CHAT, NavDest.NEWS_FEED, NavDest.MY_FRIENDS};
    private Fragment f;

    public NavDrawerAdapter(Fragment f){
        this.f = f;
    }
    @Override
    public int getCount() {
        return navItems.length;
    }

    @Override
    public NavDest getItem(int position) {
        return navItems[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        NavDest navItem = getItem(position);

        View itemView;

        if(navItem.isAHeader()){
            itemView = LayoutInflater.from(context).inflate(
                    R.layout.nav_drawer_item_header, parent, false);
            SharedPreferences settings = f.getActivity().getPreferences(Context.MODE_PRIVATE);
            String savedProfilePhotoPath = settings.getString(ProfileActivity.PREFS_PROFILE_PHOTO_PATH, null);

            if(savedProfilePhotoPath!=null){
                RoundedImageView roundedImageView = (RoundedImageView)itemView.findViewById(R.id.iv_profile_pic);

//                Picasso.with(f.getActivity()).load(savedProfilePhotoPath).into(roundedImageView);
                new ShowImage().displayPhoto(roundedImageView, savedProfilePhotoPath);
            }

        }else{
            ListView lv = (ListView)parent;
            itemView = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_activated_1, parent, false);

            TextView tv = (TextView) itemView.findViewById(android.R.id.text1);
            tv.setText(navItem.getStrResId());
        }
        return itemView;
    }
}
