package client.mytango.com.mytango;

/**
 * Created by huf on 27/10/2014.
 */
public enum NavDest {

    HEADER(-1, NavItemType.HEADER),
    CHAT(R.string.nav_chat, NavItemType.LABEL),
    NEWS_FEED(R.string.nav_news_feed, NavItemType.LABEL),
    MY_FRIENDS(R.string.nav_my_friends, NavItemType.LABEL);

    private int strResId;
    private NavItemType type;

    private NavDest(int strResId, NavItemType type){
        this.strResId = strResId;
        this.type = type;
    }

    public int getStrResId(){
        return strResId;
    }

    public boolean isALable(){
        return type == NavItemType.LABEL;
    }

    public boolean isAHeader(){
        return type == NavItemType.HEADER;
    }
}
