package client.mytango.com.mytango;

import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by huf on 30/10/2014.
 */
public class CreateImageFile {

    private static CreateImageFile instance = null;

    private static final String JPEG_FILE_PREFIX = "Photo";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private static final String ALBUM_NAME = "MediaPage";

    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

    private String photoPath;

    public static CreateImageFile getInstance(){
        if(instance == null){
            instance = new CreateImageFile();
        }
        return instance;
    }

    public File getImageFile()throws IOException{
            // Create an image file name
            String imageFileName = JPEG_FILE_PREFIX;
            File albumF = getAlbumDir();
            File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
            photoPath = imageF.getAbsolutePath();
            return imageF;
    }

    public String getPhotoPath(){
        return photoPath;
    }

    /* Photo album for this application */
    private String getAlbumName() {
        return ALBUM_NAME;
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (! storageDir.mkdirs()) {
                    if (! storageDir.exists()){
                        if (BuildConfig.DEBUG) Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            if (BuildConfig.DEBUG) Log.v("MyTango", "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }


}
