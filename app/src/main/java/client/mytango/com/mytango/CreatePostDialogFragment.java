package client.mytango.com.mytango;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import java.util.Date;

import client.mytango.com.dummydate.MediaContent;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.User;
import client.mytango.com.networking.ApiManager;
import rx.Observable;
import rx.android.concurrency.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by huf on 28/10/2014.
 */
public class CreatePostDialogFragment extends DialogFragment implements View.OnClickListener{
    private onPostCreatedListener mListener;

    private static final int ACTION_TAKE_VIDEO = 1;
    private final int SELECT_IMAGE_FROM_GALLERY = 2;
    private final int SELECT_IMAGE_FROM_CAMERA = 3;

    private LinearLayout actionsBar;
    private RelativeLayout postMedia;
    private Uri mVideoUri = null;
    private String photoPath = null;
    private EditText et_going_on;

    public static final String POST_KEY_TEXT = "text";
    public static final String POST_KEY_PICTURE_PATH = "picture";
    public static final String POST_KEY_VIDEO_PATH = "video";

    private String profileName;

    public CreatePostDialogFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            profileName = getArguments().getString(ProfileActivity.KEY_PROFILE_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View dialogFragmentView = inflater.inflate(R.layout.dialog_create_post, container, false);
        et_going_on = (EditText) dialogFragmentView.findViewById(R.id.et_going_on);
        ImageView send_now = (ImageView) dialogFragmentView.findViewById(R.id.action_send_now);
        send_now.setOnClickListener(this);

        ImageView takeVideo = (ImageView) dialogFragmentView.findViewById(R.id.action_video);
        takeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakeVideoIntent();
            }
        });

        ImageView choose_picture = (ImageView) dialogFragmentView.findViewById(R.id.action_picture);
        choose_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_IMAGE_FROM_GALLERY);
            }
        });

        ImageView take_picture = (ImageView) dialogFragmentView.findViewById(R.id.action_camera);
        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            try{
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(CreateImageFile.getInstance().getImageFile()));
                startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
            }catch(Exception e){

            }
            }
        });

        actionsBar = (LinearLayout) dialogFragmentView.findViewById(R.id.actions_bar);
        postMedia = (RelativeLayout) dialogFragmentView.findViewById(R.id.post_media);

        ImageView post_remove_media = (ImageView) dialogFragmentView.findViewById(R.id.post_remove_media);
        post_remove_media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionsBar.setVisibility(View.VISIBLE);
                postMedia.setVisibility(View.GONE);

                resetMediaContent();
            }
        });

        return dialogFragmentView;
    }

    private void resetMediaContent() {
        mVideoUri = null;
        photoPath = null;
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(takeVideoIntent, ACTION_TAKE_VIDEO);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            resetMediaContent();
        if( requestCode == ACTION_TAKE_VIDEO ) {
            handleCameraVideo(data);
        }else if (requestCode == SELECT_IMAGE_FROM_CAMERA) {
            try {
                if(CreateImageFile.getInstance().getPhotoPath()!=null){
                    photoPath=CreateImageFile.getInstance().getPhotoPath();
                    displayPostImage();
                }
            }catch (Exception e){

            }

        } else if (requestCode == SELECT_IMAGE_FROM_GALLERY) {

            Uri selectedImage = data.getData();
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String picturePath = c.getString(columnIndex);
            c.close();
            photoPath = picturePath;

            displayPostImage();
        }
        }
    }

    private void displayPostImage() {
        actionsBar.setVisibility(View.GONE);
        postMedia.setVisibility(View.VISIBLE);
        ImageView postImage = (ImageView)postMedia.findViewById(R.id.post_image);
        postImage.setVisibility(View.VISIBLE);
        VideoView postVideo = (VideoView)postMedia.findViewById(R.id.post_video);
        postVideo.setVisibility(View.GONE);
        new ShowImage().displayPhoto(postImage, photoPath);

    }

    private void handleCameraVideo(Intent intent) {
        actionsBar.setVisibility(View.GONE);
        postMedia.setVisibility(View.VISIBLE);
        ImageView postImage = (ImageView)postMedia.findViewById(R.id.post_image);
        postImage.setVisibility(View.GONE);

        mVideoUri = intent.getData();
        VideoView postVideo = (VideoView)postMedia.findViewById(R.id.post_video);
        postVideo.setVisibility(View.VISIBLE);
        postVideo.setVideoURI(mVideoUri);
        postVideo.start();
    }

    @Override
    public void onClick(View v) {
        if(profileName!=null){
            MediaContent mediaContent = new MediaContent(et_going_on.getText().toString(), photoPath, mVideoUri);

            Post createdPost = new Post();

            createdPost.setMediaContent(mediaContent);
            createdPost.setUserName(profileName);
            Date d = new Date();
            createdPost.setMillis(d.getTime());

            ApiManager.createPostData(createdPost);

        }


        if (mListener != null) {
            mListener.onPostCreated();
        }

        getDialog().dismiss();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onPostCreatedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface onPostCreatedListener {
        public void onPostCreated();
    }
}
