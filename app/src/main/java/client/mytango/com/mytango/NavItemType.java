package client.mytango.com.mytango;

/**
 * Created by huf on 27/10/2014.
 */
public enum NavItemType {
    HEADER, LABEL;
}
