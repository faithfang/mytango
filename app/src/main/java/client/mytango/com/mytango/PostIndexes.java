package client.mytango.com.mytango;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huf on 30/10/2014.
 */
public class PostIndexes {
    private static PostIndexes instance = null;

    private List<String> postIndexes = new ArrayList<String>();
    private int postIndex = 0;

    public static PostIndexes getInstance(){
        if(instance == null){
            instance = new PostIndexes();
        }
        return instance;
    }

    public List<String> getPostIndexes(){
        return postIndexes;
    }

    public int getPostIndex(){
        return postIndex;
    }

    public void increasePostIndex(){
        ++postIndex;
    }
}
