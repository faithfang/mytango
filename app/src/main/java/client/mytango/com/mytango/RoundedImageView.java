package client.mytango.com.mytango;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by huf on 27/10/2014.
 */
public class RoundedImageView extends ImageView {
    int shadowColor;
    int photoDiameter;

    public RoundedImageView(Context context) {
        super(context);

        init(context.getResources());
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context.getResources());
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(context.getResources());
    }

    private void init(Resources res) {
        shadowColor = res.getColor(R.color.dark_green_2);
        photoDiameter = Float.valueOf(
                res.getDimension(R.dimen.menu_photo_diameter)).intValue();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        Bitmap rounded = getRoundedCornerBitmap(bm, getContext());
        this.setImageDrawable(new BitmapDrawable(getContext().getResources(),
                rounded));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public Bitmap getRoundedCornerBitmap(Bitmap bitmap, Context context) {

        Bitmap output = Bitmap.createBitmap(photoDiameter, photoDiameter,
                Bitmap.Config.ARGB_8888);
        try {
            // Scale into bitmap
            Bitmap resizedbitmap = Bitmap.createScaledBitmap(bitmap, photoDiameter,
                    photoDiameter, true);

            Canvas canvas = new Canvas(output);

            final Paint paint = new Paint();

            final Rect rect = new Rect(0, 0, photoDiameter, photoDiameter);

            paint.setAntiAlias(true);

            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(shadowColor);

            int radius = photoDiameter / 2;

            canvas.drawCircle(radius, radius, radius, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

            canvas.drawBitmap(resizedbitmap, rect, rect, paint);

            // Draw frame shadow outline, offset slightly bigger circle
            final Rect oval = new Rect(0, 1, photoDiameter + 2, photoDiameter + 2);
            final RectF ovalF = new RectF(oval);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(3);
            canvas.drawArc(ovalF, 160F, 290F, false, paint);
        }catch(Exception e){

        }

        return output;

    }
}
