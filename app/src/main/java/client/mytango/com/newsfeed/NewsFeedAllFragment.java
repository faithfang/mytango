package client.mytango.com.newsfeed;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import client.mytango.com.mytango.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class NewsFeedAllFragment extends Fragment {

    public NewsFeedAllFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_feed_all, container, false);
    }


}
