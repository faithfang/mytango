package client.mytango.com.newsfeed;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import client.mytango.com.dummydate.Comment;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.Posts;
import client.mytango.com.dummydate.User;
import client.mytango.com.dummydate.Users;
import client.mytango.com.mytango.R;
import client.mytango.com.networking.ApiManager;
import rx.Observable;
import rx.android.concurrency.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class FeedDetailActivity extends ActionBarActivity implements FeedDetailAdapter.ItemClickListener{
    private String postUsername;
    private long postTime = -1;

    private RecyclerView recyclerView;

    private Users mUsers;
    private Posts mPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        postUsername = getIntent().getExtras().getString(NewsFeedChannelsFragment.KEY_POST_USER_NAME);

        postTime = getIntent().getExtras().getLong(NewsFeedChannelsFragment.KEY_POST_MILLIS);

        setContentView(R.layout.activity_feed_detail);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView)findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }



    @Override
    public void onResume() {
        super.onResume();
        //get users
        //get posts

        Observable.from("")
                .flatMap(new Func1<String, Observable<Users>>() {
                    @Override
                    public Observable<Users> call(String s) {
                        return ApiManager.getUsersData(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Users>() {
                    @Override
                    public void call(Users users) {
                        // do your work
                        mUsers = users;
                        updateUI();
                    }
                });

        Observable.from("")
                .flatMap(new Func1<String, Observable<Posts>>() {
                    @Override
                    public Observable<Posts> call(String s) {
                        return ApiManager.getPostsData(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Posts>() {
                    @Override
                    public void call(Posts posts) {
                        // do your work
                        mPosts = posts;
                        updateUI();
                    }
                });

    }

    private void updateUI() {
        if (mUsers == null || mPosts == null) {
            return;
        }

        //get current post
        //get user
        User pUser = null;
        if(postUsername!=null){
            pUser = getUser(postUsername);

        }


        Post pPost = null;
        if(postTime!=-1){
            pPost = getPost(postTime);

        }
        if(pUser!=null && pPost!=null){
            List<Comment> commentList = null;
            if(pPost.getComments()!=null){
                commentList = Arrays.asList(pPost.getComments());
                Collections.sort(commentList);
                Collections.reverse(commentList);
            }

            FeedDetailAdapter commentsAdapter = new FeedDetailAdapter(this, pUser, pPost, commentList, mUsers.getUsers(), this);
            recyclerView.setAdapter(commentsAdapter);
        }
    }

    private Post getPost(long time){
        Post p = null;
        if(mPosts!=null && mPosts.getPosts()!=null){
            for(Post aPost:mPosts.getPosts()){
                if(aPost.getMillis()==time){
                    p = aPost;
                    break;
                }
            }
        }
        return p;
    }

    private User getUser(String name){
        User us = null;
        if(mUsers!=null&&mUsers.getUsers()!=null){
            for(User u:mUsers.getUsers()){
                if(u.getName().equals(name)){
                    us = u;
                    break;
                }
            }
        }

        return us;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()){
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void itemClicked(Comment item) {
        //Do nothing
    }
}
