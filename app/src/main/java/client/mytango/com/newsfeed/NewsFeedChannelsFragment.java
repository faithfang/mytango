package client.mytango.com.newsfeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.Posts;
import client.mytango.com.dummydate.User;
import client.mytango.com.dummydate.Users;
import client.mytango.com.mytango.R;
import client.mytango.com.networking.ApiManager;
import rx.Observable;
import rx.android.concurrency.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class NewsFeedChannelsFragment extends Fragment implements FeedAdapter.ItemClickListener, FeedAdapter.RunnableAddedListener{
    public static final String KEY_POST_USER_NAME = "post_user_name";
    public static final String KEY_POST_MILLIS = "post_millis";

    private Users mUsers;
    private Posts mPosts;

    private String owner = "Chloe";
    private RecyclerView recyclerView;

    // A queue of Runnables
    private final BlockingQueue<Runnable> mDecodeWorkQueue = new LinkedBlockingQueue<Runnable>();

    private Timer timer;

    public NewsFeedChannelsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_feed_channels, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        ImageView overlay = (ImageView) view.findViewById(R.id.overlay);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnItemTouchListener(new DragController(recyclerView, overlay));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //get users
        //get posts

        Observable.from("")
                .flatMap(new Func1<String, Observable<Users>>() {
                    @Override
                    public Observable<Users> call(String s) {
                        return ApiManager.getUsersData(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Users>() {
                    @Override
                    public void call(Users users) {
                        // do your work
                        mUsers = users;
                        updateUI();
                    }
                });

        Observable.from("")
                .flatMap(new Func1<String, Observable<Posts>>() {
                    @Override
                    public Observable<Posts> call(String s) {
                        return ApiManager.getPostsData(s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Posts>() {
                    @Override
                    public void call(Posts posts) {
                        // do your work
                        mPosts = posts;
                        updateUI();
                    }
                });

    }

    private void updateUI(){
        if(mUsers==null||mPosts==null){
            return;
        }

        //update UI

        //get current user
        //get followings
        //get posts by the followings
        //sort posts
        //combine post with name and image
        //display post from the newest
        String[] lFollowings = null;
        for(User littleUser: mUsers.getUsers()){
            if(littleUser.getName().equals(owner)){
                lFollowings = littleUser.getFollowingNames();
            }
        }
        if(lFollowings!=null){
            List<String> followingsList = Arrays.asList(lFollowings);

            List<Post> lPosts = new ArrayList<Post>();

            for(Post lPost:mPosts.getPosts()){
                if(followingsList.contains(lPost.getUserName())){
                    lPosts.add(lPost);
                }
            }
            if(lPosts.size()!=0){
                Collections.sort(lPosts);
                Collections.reverse(lPosts);

                FeedAdapter adapter = new FeedAdapter(getActivity(), lPosts, mUsers.getUsers(), NewsFeedChannelsFragment.this, NewsFeedChannelsFragment.this);
                recyclerView.setAdapter(adapter);

                TimerTask task = new TimerTask(){
                    public void run() {
                        if(mDecodeWorkQueue.size()!=0){
                            Iterator itr = mDecodeWorkQueue.iterator();
                            while(itr.hasNext()) {
                                try{
                                    Runnable element = (Runnable)itr.next();
                                    if(element!=null){
                                        getActivity().runOnUiThread(element);
                                    }
                                }catch (Exception e){

                                }

                            }
                        }
                    }
                };

                timer = new Timer();
                timer.scheduleAtFixedRate(task, 0, 4000);
            }
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(timer!=null){
            timer.cancel();
        }
    }

    @Override
    public void itemClicked(String name, long millis) {
        Intent detailIntent = new Intent(getActivity(), FeedDetailActivity.class);
        detailIntent.putExtra(KEY_POST_USER_NAME, name);
        detailIntent.putExtra(KEY_POST_MILLIS, millis);
        startActivity(detailIntent);
    }

    @Override
    public void runnableAdded(Runnable item) {
//        Intent detailIntent = new Intent(FeedListActivity.this, FeedDetailActivity.class);
//        detailIntent.putExtra(FeedDetailActivity.ARG_ITEM, item);
//        startActivity(detailIntent);
        mDecodeWorkQueue.add(item);

    }
}
