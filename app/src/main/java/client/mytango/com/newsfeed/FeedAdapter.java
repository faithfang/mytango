package client.mytango.com.newsfeed;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import client.mytango.com.dummydate.Comment;
import client.mytango.com.dummydate.MediaContent;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.User;
import client.mytango.com.mytango.R;
import client.mytango.com.mytango.RoundedImageView;
import client.mytango.com.mytango.ShowImage;
import client.mytango.com.utils.TimeAgo;

/**
 * Created by huf on 26/11/2014.
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder>{
    protected LayoutInflater inflater;
    List<Post> lPosts = new ArrayList<Post>();
    Context context;
    static int curIndex = 0;
    Comment[] tComments;
    User[] usersArray;
    private ItemClickListener itemClickListener;
    private RunnableAddedListener runnableAddedListener;


    public interface ItemClickListener {
        public void itemClicked(String name, long mills);
    }

    public interface RunnableAddedListener{
        public void runnableAdded(Runnable run);
    }

    public FeedAdapter(Context context, List<Post> lPosts, User[] usersArray, @NonNull ItemClickListener itemClickListener, @NonNull RunnableAddedListener runnableAddedListener){
        this.lPosts = lPosts;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.usersArray = usersArray;
        this.itemClickListener = itemClickListener;
        this.runnableAddedListener = runnableAddedListener;
        setHasStableIds(true);
    }

    //Stable IDs mean that if we request an ID for a location, any specific item will always return the same ID regardless of its position within the list.
    @Override
    public long getItemId(int position) {
        //assume post date is unique
        return lPosts.get(position).getMillis();
    }

    public void moveItem(int start, int end) {
        int max = Math.max(start, end);
        int min = Math.min(start, end);
        if (min >= 0 && max < lPosts.size()) {
            Post item = lPosts.remove(min);
            lPosts.add(max, item);
            notifyItemMoved(min, max);
        }
    }

    public int getPositionForId(long id) {
        for (int i = 0; i < lPosts.size(); i++) {
            if (lPosts.get(i).getMillis() == id) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return lPosts.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        View parent = inflater.inflate(R.layout.post_block, viewGroup, false);
        return ViewHolder.newInstance(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Post item = lPosts.get(position);
        User us = getUser(item.getUserName());

        viewHolder.setHeadImage(us.getPhotoPath());
        viewHolder.setName(item.getUserName());
        viewHolder.setPostDate(item.getMillis());
        viewHolder.setMediaContent(item.getMediaContent());
        viewHolder.setLikes(item.getLikes());
        runnableAddedListener.runnableAdded(viewHolder.setComments(item.getComments()));

        viewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.itemClicked(item.getUserName(), item.getMillis());
            }
        });
    }

    private User getUser(String name){
        User us = null;
        if(usersArray!=null){
            for(User u:usersArray){
                if(u.getName().equals(name)){
                    us = u;
                    break;
                }
            }
        }

        return us;
    }



    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private final View parent;
        private final RoundedImageView headImage;
        private final TextView name;
        private final TextView postDate;
        private final TextView text;
        private final ImageView image;
        private final VideoView video;
        private final TextView likes;
        private final TextView commentsAmount;
        private final TextSwitcher commentsTextSwitcher;


        public static ViewHolder newInstance(View parent) {
            RoundedImageView headImage = (RoundedImageView)parent.findViewById(R.id.profile_pic);
            TextView name = (TextView)parent.findViewById(R.id.name);
            TextView postDate = (TextView)parent.findViewById(R.id.time);
            TextView text = (TextView)parent.findViewById(R.id.textView);
            ImageView image = (ImageView)parent.findViewById(R.id.imageView);
            VideoView video = (VideoView)parent.findViewById(R.id.videoView);
            TextView likes = (TextView)parent.findViewById(R.id.likes);
            TextView commentsAmount = (TextView)parent.findViewById(R.id.commentsAmount);
            TextSwitcher commentsTextSwitcher = (TextSwitcher) parent.findViewById(R.id.commentsTextSwitcher);

            return new ViewHolder(parent, headImage, name, postDate, text, image, video, likes, commentsAmount, commentsTextSwitcher);
        }

        private ViewHolder(View parent, RoundedImageView headImage, TextView name, TextView postDate, TextView text, ImageView image, VideoView video, TextView likes, TextView commentsAmount, TextSwitcher commentsTextSwitcher) {
            super(parent);
            this.parent = parent;
            this.headImage = headImage;
            this.name = name;
            this.postDate = postDate;
            this.text = text;
            this.image = image;
            this.video = video;
            this.likes = likes;
            this.commentsAmount = commentsAmount;
            this.commentsTextSwitcher = commentsTextSwitcher;
        }

        public Runnable setComments(final Comment[] comments){
            Runnable setTextRunnable = null;
            if(comments!=null){

                commentsAmount.setText(String.valueOf(comments.length));
                commentsAmount.setVisibility(View.VISIBLE);

                AlphaAnimation fadeIn = new AlphaAnimation(0.0f , 1.0f ) ;
                AlphaAnimation fadeOut = new AlphaAnimation( 1.0f , 0.0f ) ;

                fadeIn.setDuration(1000);
                fadeIn.setFillAfter(true);

                fadeOut.setDuration(1000);
                fadeOut.setFillAfter(true);
                fadeOut.setStartOffset(2000+fadeIn.getStartOffset());

                commentsTextSwitcher.setInAnimation(fadeIn);
                commentsTextSwitcher.setOutAnimation(fadeOut);

                if (commentsTextSwitcher.getChildCount() != 2) {
                    commentsTextSwitcher.removeAllViews();
                    commentsTextSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

                        @Override
                        public View makeView() {
                            TextView textView = new TextView(commentsTextSwitcher.getContext());
                            return textView;
                        }
                    });
                }

                setTextRunnable = new Runnable() {
                    public void run() {

                        if(curIndex >= comments.length-1){
                            curIndex = 0;
                            commentsTextSwitcher.setText(comments[curIndex].getSaid());
                        }else {
                            commentsTextSwitcher.setText(comments[++curIndex].getSaid());
                        }
                    }

                };

            }
            return setTextRunnable;


        }

        public void setLikes(Integer likes1){
            if(likes1!=null){
                likes.setText("+" + likes1);
                likes.setVisibility(View.VISIBLE);
            }
        }

        public void setMediaContent(MediaContent mc){

            if(mc!=null){
                if(mc.getPlainText()!=null){
                    text.setText(mc.getPlainText());
                    text.setVisibility(View.VISIBLE);
                }
                if(mc.getImagePath()!=null){
                    new ShowImage().displayPhoto(image, mc.getImagePath());
                    image.setVisibility(View.VISIBLE);
                }
                if(mc.getVideoUri()!=null){
                    video.setVideoURI(mc.getVideoUri());
                    video.seekTo(100);
                    video.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            video.start();
                            return false;
                        }
                    });

                    video.setVisibility(View.VISIBLE);
                }

            }
        }

        public void setHeadImage(String imagePath) {
            if(imagePath!=null) {
                new ShowImage().displayPhoto(headImage, imagePath);
            }
        }

        public void setName(String text) {
            name.setText(text);
        }

        public void setPostDate(long millis) {
            postDate.setText(TimeAgo.getTimeAgo(millis));
        }

        public void setOnClickListener(View.OnClickListener listener) {
            parent.setOnClickListener(listener);
        }
    }

}
