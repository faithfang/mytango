package client.mytango.com.newsfeed;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;
import java.util.List;

import client.mytango.com.dummydate.Comment;
import client.mytango.com.dummydate.MediaContent;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.User;
import client.mytango.com.mytango.R;
import client.mytango.com.mytango.RoundedImageView;
import client.mytango.com.mytango.ShowImage;
import client.mytango.com.utils.TimeAgo;

/**
 * Created by huf on 19/12/2014.
 */
public class FeedDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    protected LayoutInflater inflater;
    Context context;
    User user;
    Post post;
    List<Comment> lComments = null;
    User[] usersArray;
    private ItemClickListener itemClickListener;

    public FeedDetailAdapter(Context context, User user, Post post, List<Comment> lComments, User[] usersArray, @NonNull ItemClickListener itemClickListener){
        this.user = user;
        this.post = post;
        this.lComments = lComments;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.usersArray = usersArray;
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        public void itemClicked(Comment item);
    }

    @Override
    public int getItemCount() {
        if(lComments==null){
            return 1;
        }else{
            return lComments.size() + 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if(viewType == TYPE_ITEM){
            View parent = inflater.inflate(R.layout.comment, viewGroup, false);
            return FeedCommentViewHolder.newInstance(parent);
        }else if(viewType == TYPE_HEADER){
            View parent = inflater.inflate(R.layout.post_detail_head, viewGroup, false);
            return FeedContentViewHolder.newInstance(parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if(viewHolder instanceof FeedCommentViewHolder){
            final Comment item = lComments.get(position-1);
            User addedComment = getUser(item.getName());
            if(addedComment!=null){
                ((FeedCommentViewHolder)viewHolder).setHeadImage(addedComment.getPhotoPath());
                ((FeedCommentViewHolder)viewHolder).setName(item.getName());
            }
            ((FeedCommentViewHolder)viewHolder).setPostDate(item.getMillis());
            ((FeedCommentViewHolder)viewHolder).setCommentText(item.getSaid());


        }else if(viewHolder instanceof FeedContentViewHolder){

            ((FeedContentViewHolder)viewHolder).setHeadImage(user.getPhotoPath());
            ((FeedContentViewHolder)viewHolder).setName(user.getName());
            ((FeedContentViewHolder)viewHolder).setPostDate(post.getMillis());
            ((FeedContentViewHolder)viewHolder).setMediaContent(post.getMediaContent());
            ((FeedContentViewHolder)viewHolder).setLikes(post.getLikes());
            ((FeedContentViewHolder)viewHolder).setComments(post.getComments());
        }
    }




    public static final class FeedContentViewHolder extends RecyclerView.ViewHolder{
        private final View parent;
        private final RoundedImageView headImage;
        private final TextView name;
        private final TextView postDate;
        private final TextView text;
        private final ImageView image;
        private final VideoView video;
        private final TextView likes;
        private final TextView commentsAmount;


        public static FeedContentViewHolder newInstance(View parent) {
            RoundedImageView headImage = (RoundedImageView)parent.findViewById(R.id.profile_pic);
            TextView name = (TextView)parent.findViewById(R.id.name);
            TextView postDate = (TextView)parent.findViewById(R.id.time);
            TextView text = (TextView)parent.findViewById(R.id.textView);
            ImageView image = (ImageView)parent.findViewById(R.id.imageView);
            VideoView video = (VideoView)parent.findViewById(R.id.videoView);
            TextView likes = (TextView)parent.findViewById(R.id.likes);
            TextView commentsAmount = (TextView)parent.findViewById(R.id.commentsAmount);

            return new FeedContentViewHolder(parent, headImage, name, postDate, text, image, video, likes, commentsAmount);
        }

        private FeedContentViewHolder(View parent, RoundedImageView headImage, TextView name, TextView postDate, TextView text, ImageView image, VideoView video, TextView likes, TextView commentsAmount) {
            super(parent);
            this.parent = parent;
            this.headImage = headImage;
            this.name = name;
            this.postDate = postDate;
            this.text = text;
            this.image = image;
            this.video = video;
            this.likes = likes;
            this.commentsAmount = commentsAmount;
        }

        public void setComments(final Comment[] comments){
            if(comments!=null) {

                commentsAmount.setText(String.valueOf(comments.length));
                commentsAmount.setVisibility(View.VISIBLE);
            }
        }

            public void setLikes(Integer likes1){
            if(likes1!=null){
                likes.setText("+" + likes1);
                likes.setVisibility(View.VISIBLE);
            }
        }

        public void setMediaContent(MediaContent mc){

            if(mc!=null){
                if(mc.getPlainText()!=null){
                    text.setText(mc.getPlainText());
                    text.setVisibility(View.VISIBLE);
                }
                if(mc.getImagePath()!=null){
                    new ShowImage().displayPhoto(image, mc.getImagePath());
                    image.setVisibility(View.VISIBLE);
                }
                if(mc.getVideoUri()!=null){
                    video.setVideoURI(mc.getVideoUri());
                    video.seekTo(100);
                    video.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            video.start();
                            return false;
                        }
                    });

                    video.setVisibility(View.VISIBLE);
                }

            }
        }

        public void setHeadImage(String imagePath) {
            if(imagePath!=null) {
                new ShowImage().displayPhoto(headImage, imagePath);
            }
        }

        public void setName(String text) {
            name.setText(text);
        }

        public void setPostDate(long millis) {
            postDate.setText(TimeAgo.getTimeAgo(millis));
        }
    }


    private User getUser(String name){
        User us = null;
        if(usersArray!=null){
            for(User u:usersArray){
                if(u.getName().equals(name)){
                    us = u;
                    break;
                }
            }
        }

        return us;
    }




    public static final class FeedCommentViewHolder extends RecyclerView.ViewHolder {
        private final View parent;
        private final RoundedImageView headImage;
        private final TextView name;
        private final TextView postDate;
        private final TextView text;


        public static FeedCommentViewHolder newInstance(View parent) {
            RoundedImageView headImage = (RoundedImageView)parent.findViewById(R.id.profile_pic);
            TextView name = (TextView)parent.findViewById(R.id.name);
            TextView postDate = (TextView)parent.findViewById(R.id.time);
            TextView text = (TextView)parent.findViewById(R.id.text);

            return new FeedCommentViewHolder(parent, headImage, name, postDate, text);
        }

        private FeedCommentViewHolder(View parent, RoundedImageView headImage, TextView name, TextView postDate, TextView text) {
            super(parent);
            this.parent = parent;
            this.headImage = headImage;
            this.name = name;
            this.postDate = postDate;
            this.text = text;
        }

        public void setCommentText(String commentText){
            text.setText(commentText);
        }

        public void setHeadImage(String imagePath) {
            if(imagePath!=null) {
                new ShowImage().displayPhoto(headImage, imagePath);
            }
        }

        public void setName(String text) {
            name.setText(text);
        }

        public void setPostDate(long millis) {
            postDate.setText(TimeAgo.getTimeAgo(millis));
        }

    }
}
