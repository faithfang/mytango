package client.mytango.com.newsfeed;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;

import client.mytango.com.mytango.BaseActivity;
import client.mytango.com.mytango.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class NewsFeedFragment extends Fragment {

    private ViewPager mViewPager;

    public NewsFeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View content = inflater.inflate(R.layout.fragment_news_feed, container, false);

        final TabHost tabHost = (TabHost)content.findViewById(R.id.tabHost);
        tabHost.setup();

        String tab_all = tabHost.getContext().getResources().getString(R.string.tab_all);
        TabHost.TabSpec spec1 = tabHost.newTabSpec(tab_all);
        spec1.setIndicator(tab_all);
        spec1.setContent(R.id.contentView1);
        String tab_my_friends = tabHost.getContext().getResources().getString(R.string.tab_my_friends);
        TabHost.TabSpec spec2 = tabHost.newTabSpec(tab_my_friends);
        spec2.setIndicator(tab_my_friends);
        spec2.setContent(R.id.contentView2);
        String tab_channels = tabHost.getContext().getResources().getString(R.string.tab_channels);
        TabHost.TabSpec spec3 = tabHost.newTabSpec(tab_channels);
        spec3.setIndicator(tab_channels);
        spec3.setContent(R.id.contentView3);
        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);

//        MerchantFragment.makeTabsSameAsActionBar(tabHost.getTabWidget());

        tabHost.setOnTabChangedListener(new OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                int position = tabHost.getCurrentTab();
                mViewPager.setCurrentItem(position);
            }
        });


        mViewPager=(ViewPager)content.findViewById(R.id.pager);
        mViewPager.setAdapter(new MorePagerAdapter(getChildFragmentManager()));
        mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
                // Unfortunately when TabHost changes the current tab, it kindly also takes care of
                // putting focus on it when not in touch mode. The jerk. This hack tries to prevent
                // this from pulling focus out of our ViewPager.
                TabWidget widget = tabHost.getTabWidget();
                int oldFocusability = widget.getDescendantFocusability();
                widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
                tabHost.setCurrentTab(arg0);
                widget.setDescendantFocusability(oldFocusability);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        return content;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((BaseActivity) activity).onSectionAttached(
                getArguments().getInt(BaseActivity.ARG_SECTION_NUMBER));
    }

    private class MorePagerAdapter extends FragmentStatePagerAdapter {
        public MorePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new NewsFeedAllFragment();
                case 1:
                    return new NewsFeedMyFriendsFragment();
                case 2:
                    return new NewsFeedChannelsFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
