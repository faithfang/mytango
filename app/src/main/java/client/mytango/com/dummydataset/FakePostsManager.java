package client.mytango.com.dummydataset;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import client.mytango.com.application.MApplication;
import client.mytango.com.application.Prefs;
import client.mytango.com.dummydate.Comment;
import client.mytango.com.dummydate.MediaContent;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.Posts;

/**
 * Created by huf on 25/11/2014.
 */
public class FakePostsManager {

    //compose post one
    private static Calendar cCalendar = new GregorianCalendar(114 + 1900, 1, 2, 1, 1, 1);
    private static Comment comment = new Comment("Chloe", "Most these startups donot know how to Android.", cCalendar.getTimeInMillis());
    private static Comment[] comments = {comment};
    private static String postText = "Please Follow:+Words of Wisdom";
    private static MediaContent mediaContent = new MediaContent(postText, null, null);
    private static Integer likes = 6;
    private static Calendar pCalendar = new GregorianCalendar(114 + 1900, 1, 1, 1, 1, 1);
    private static long millis = pCalendar.getTimeInMillis();
    private static String userName = "Chloe";
    private static Post postOne = new Post(userName, millis, mediaContent, likes, comments);

    //compose post two
    private static Calendar cCalendar2 = new GregorianCalendar(114 + 1900, 3, 2, 1, 1, 1);
    private static Comment comment2 = new Comment("Chloe", "Looks like it is great.", cCalendar2.getTimeInMillis());
    private static Comment[] comments2 = {comment2};
    private static String postText2 = "LOLlipop design details are winning over some Apple users as well.";
    private static MediaContent mediaContent2 = new MediaContent(postText2, null, null);
    private static Integer likes2 = 6;
    private static Calendar pCalendar2 = new GregorianCalendar(114 + 1900, 2, 1, 1, 1, 1);
    private static long millis2 = pCalendar2.getTimeInMillis();
    private static String userName2 = "Mia";
    private static Post postTwo = new Post(userName2, millis2, mediaContent2, likes2, comments2);

    //compose post three
    private static Calendar cCalendar3 = new GregorianCalendar(114 + 1900, 4, 2, 1, 1, 1);
    private static Comment comment3 = new Comment("Chloe", "Looks like it is great.", cCalendar3.getTimeInMillis());
    private static Comment comment31 = new Comment("Mia", "I think this is especially dumb this time.", cCalendar3.getTimeInMillis());
    private static Comment[] comments3 = {comment3, comment31};
    private static String postText3 = "Europe is going after Google again.";
    private static MediaContent mediaContent3 = new MediaContent(postText3, null, null);
    private static Integer likes3 = 3;
    private static Calendar pCalendar3 = new GregorianCalendar(114 + 1900, 2, 2, 1, 1, 1);
    private static long millis3 = pCalendar3.getTimeInMillis();
    private static String userName3 = "Mia";
    private static Post postThree = new Post(userName3, millis3, mediaContent3, likes3, comments3);

    //compose post four
    private static Calendar cCalendar4 = new GregorianCalendar(114 + 1900, 5, 2, 1, 1, 1);
    private static Comment comment4 = new Comment("Chloe", "Looks like it is great.", cCalendar4.getTimeInMillis());
    private static Comment comment41 = new Comment("Mia", "I think this is especially dumb this time.", cCalendar4.getTimeInMillis());
    private static Comment[] comments4 = {comment4, comment41};
    private static String postText4 = "Europe is going after Google again.";
    private static MediaContent mediaContent4 = new MediaContent(postText4, null, null);
    private static Integer likes4 = 3;
    private static Calendar pCalendar4 = new GregorianCalendar(114 + 1900, 3, 1, 1, 1, 1);
    private static long millis4 = pCalendar4.getTimeInMillis();
    private static String userName4 = "Chloe";
    private static Post postFour = new Post(userName4, millis4, mediaContent4, likes4, comments4);

    //compose post five
    private static Calendar cCalendar5 = new GregorianCalendar(114 + 1900, 6, 2, 1, 1, 1);
    private static Calendar cCalendar51 = new GregorianCalendar(114 + 1900, 6, 2, 2, 1, 1);
    private static Comment comment5 = new Comment("Chloe", "I know you can do it.", cCalendar5.getTimeInMillis());
    private static Comment comment51 = new Comment("Mia", "Well done.", cCalendar51.getTimeInMillis());
    private static Comment[] comments5 = {comment5, comment51};
    private static String postText5 = "Bring! is one of the most useful apps I have. With the new update there's even more features that I wanted to see. \n\nbut why, oh why is Google login not supported.";
    private static MediaContent mediaContent5 = new MediaContent(postText5, null, null);
    private static Integer likes5 = 10;
    private static Calendar pCalendar5 = new GregorianCalendar(114 + 1900, 5, 1, 1, 1, 1);
    private static long millis5 = pCalendar5.getTimeInMillis();
    private static String userName5 = "Benjamin";
    private static Post postFive = new Post(userName5, millis5, mediaContent5, likes5, comments5);

    private Gson gson;
    private Type listType;

    private FakePostsManager() {
        gson = new Gson();
        listType = new TypeToken<List<Post>>() {}.getType();

        if(getPostsListFromPrefs().size()==0){
            addPost(postOne);
            addPost(postTwo);
            addPost(postThree);
            addPost(postFour);
            addPost(postFive);
        }
    }

    private static FakePostsManager mInstance = new FakePostsManager();
    public static FakePostsManager getInstance() {
        return mInstance;
    }

    public Posts getPosts(){
        Post pots[] = new Post[getPostsListFromPrefs().size()];
        pots = getPostsListFromPrefs().toArray(pots);
        return new Posts(pots);
    }

    public Posts getPostsByName(String name){
        List<Post> filteredPosts = new ArrayList<Post>();
        for(Post post:getPostsListFromPrefs()){
            if(post.getUserName().equals(name)){
                filteredPosts.add(post);
            }
        }

        Post postsArray[] = new Post[filteredPosts.size()];
        postsArray = filteredPosts.toArray(postsArray);

        return new Posts(postsArray);
    }

    public void addPost(Post po){
        List<Post> target2 = getPostsListFromPrefs();
        target2.add(po);
        savePostsList(target2);
    }

    private List<Post> getPostsListFromPrefs() {
        String postsjson = MApplication.getInstance().getPrefs().get(Prefs.PREFS_KEY_POSTS_LIST);
        if(gson.fromJson(postsjson, listType)==null){
            return new ArrayList<Post>();
        }else{
            return gson.fromJson(postsjson, listType);
        }
    }

    private void savePostsList(List<Post> lPosts) {
        String json = gson.toJson(lPosts, listType);
        MApplication.getInstance().getPrefs().set(Prefs.PREFS_KEY_POSTS_LIST, json);
    }
}
