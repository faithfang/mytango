package client.mytango.com.dummydataset;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import client.mytango.com.application.MApplication;
import client.mytango.com.application.Prefs;
import client.mytango.com.dummydate.Post;
import client.mytango.com.dummydate.User;
import client.mytango.com.dummydate.Users;

/**
 * Created by huf on 25/11/2014.
 */
public class FakeUsersManager {
    private String[] userNames = {"Ava", "Chloe", "Mia", "Benjamin"};

    private String[] followings = {"Ava", "Mia", "Benjamin" };

    LatLng trafalgar = new LatLng(51.507351, -0.127758);
    LatLng londonLibrary = new LatLng(51.507276, -0.137479);
    LatLng downingStreet = new LatLng(51.503189, -0.127180);
    LatLng nationalTheatre = new LatLng(51.506955, -0.113554);

    private User ava = new User(userNames[0], null, null, null, "Followed Yahoo Live", trafalgar);
    private User chloe = new User(userNames[1], null, "This is goat cheese.", followings, "Be happy!", londonLibrary);
    private User mia = new User(userNames[2], null, "Fresh pepperoni.", null, "The turkey is readyyy.", downingStreet);
    private User benjamin = new User(userNames[3], null, "Cannot miss this.", null, "Very special night!", nationalTheatre);

    private Gson gson;
    private Type listType;

    private FakeUsersManager() {
        gson = new Gson();
        listType = new TypeToken<List<User>>() {}.getType();

        if(getUsersListFromPrefs().size()==0){
            addUser(ava);
            addUser(chloe);
            addUser(mia);
            addUser(benjamin);
        }
    }

    private static FakeUsersManager mInstance = new FakeUsersManager();
    public static FakeUsersManager getInstance() {
        return mInstance;
    }

    public Users getUsers(){

        User uses[] = new User[getUsersListFromPrefs().size()];
        uses = getUsersListFromPrefs().toArray(uses);
        return new Users(uses);
    }

    public User getUser(String name){
        for(User user:getUsersListFromPrefs()){
            if(user.getName().equals(name)){
               return user;
            }
        }

        return null;
    }

    public void addUser(User us){
        List<User> target2 = getUsersListFromPrefs();
        target2.add(us);
        saveUsersList(target2);
    }

    public void updateUserPhotoPath(String username, String photopath){
        if(getUser(username)!=null){
            List<User> target2 = getUsersListFromPrefs();
            for(User user:target2){
                if(user.getName().equals(username)){
                    user.setPhotoPath(photopath);
                    break;
                }
            }
            saveUsersList(target2);
        }
    }

    private List<User> getUsersListFromPrefs() {
        String users = MApplication.getInstance().getPrefs().get(Prefs.PREFS_KEY_USERS_LIST);
        if(gson.fromJson(users, listType)==null){
            return new ArrayList<User>();
        }else{
            return gson.fromJson(users, listType);
        }
    }

    private void saveUsersList(List<User> lUsers) {
        String json = gson.toJson(lUsers, listType);
        MApplication.getInstance().getPrefs().set(Prefs.PREFS_KEY_USERS_LIST, json);
    }
}
